# ここなの飯能大冒険

*******

## 概要

- 「ここなの飯能大冒険マップ」を飯能市民会館にもらいにいく。
  - <https://twitter.com/yamanosusume/status/1103921626133880833>
- ついでに、ヤマノススメ2期20話のここなちゃんが歩いたルートを歩く。

## 参考ルート

- <https://ameblo.jp/isshixi/entry-12277988325.html>
- <https://www.google.com/maps/d/viewer?mid=1pVYGA3afZHFUcUAd-Ny3NhD76uY&ll=35.84167437607157%2C139.35342038889144&z=14>
  - このルートに沿って行く。
  - 飯能駅から飯能市中央公園までのルートは特に決まってないし適当に。

## スクショ



![2](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/2.PNG)

スタート地点、飯能市中央公園。

![3](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/3.PNG)

![4](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/4.PNG)

中央公園にあると思われる遊具。

![5](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/5.PNG)

どっかの道。場所不明。

![6](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/6.PNG)

飯能市市民会館。ここでここなちゃんマップをもらう。運が悪ければ品切れ。

![7](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/7.PNG)

諏訪八幡神社。

![8](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/8.PNG)

降りるシーンしかないけど、こういう階段を上った先にさっきの社があるはず。

![9](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/9.PNG)

諏訪八幡神社を出た後のルートのどっかでこの景色が見える。入間川にかかる割岩橋が遠くに見えている。

![10](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/10.PNG)

飯能河原沿いを歩く。このシーンがここなちゃん視点かどうかは不明。

![11](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/11.PNG)

飯能中央地区行政センター。階段上って東側へ通り抜け。

![12](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/12.PNG)

同様。

![13](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/13.PNG)

矢久橋。飯能中央地区行政センターの次のカットだけど、行政センターからここまではすごく遠い（２０分くらい？）。

![14](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/14.PNG)

矢久橋は渡らずに入間川の川べりへ下りる。

![15](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/15.PNG)

川沿い、飯能大橋下。

![16](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/16.PNG)

同じく飯能大橋下？ここなちゃんが座っているU形側溝ブロックは今は同じ場所には無い。

![17](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/17.PNG)

矢颪（やおろし）凝灰岩層。飯能大橋と矢久橋が後ろに見えている？

![18](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/18.PNG)

飯能南高校。矢颪凝灰岩層のカットから３０分くらい歩く。googleマップの赤ルートに沿って歩いているならこの見え方にはならないはずなので注意。

![19](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/19.PNG)

阿須（あず）の鉄橋下。

![21](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/21.PNG)

阿須運動公園。このオブジェクトがどこかは知らない。

![23](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/23.PNG)

同じく阿須運動公園。かたつむりの水飲み場は、赤ルート上にマーキングあり。

![24](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/24.PNG)

あけぼの子どもの森公園。

![25](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/25.PNG)

どこから撮ったのかは知らない。

![26](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/26.PNG)

同様。

![27](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/28.PNG)

これは、森の家という建物の中らしい。

![29](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/29.PNG)

同様。

![30](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/30.PNG)

ここなちゃんが猫と出会うベンチだけど実在しないらしい。

![31](https://bitbucket.org/hebissb/yamanosusume/raw/master/pic/31.PNG)

終わり。帰りは北に歩いて西部池袋線 元加治駅へ。

